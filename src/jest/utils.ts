import { act } from 'react-dom/test-utils';
import _ from 'lodash';

export const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift()

            if (
                config.url.includes(url) &&
                config.method.toUpperCase() === method
            ) {
                const mockQueryParams = _(params).omit(['mobileSdkData'])
                const queryParams = _(config.params).omit(['mobileSdkData'])

                if (!queryParams || _.isEqual(queryParams, mockQueryParams)) {
                    return response
                }

                console.error(`Не обработанный запрос
Параметры не совпадают
${JSON.stringify(mockQueryParams.value(), null, 2)}
${JSON.stringify(queryParams.value(), null, 2)}`)

                return [500, {}]
            }

            console.error(`Не обработанный запрос
Адрес и(или) метод не совпадает
${JSON.stringify(config, null, 2)}`)

            return [500, {}]
        })
    })
}