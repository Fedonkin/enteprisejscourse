import React from "react";

export const TestComponent = ({children}) =>  (
        <div>{children}</div>
    );
