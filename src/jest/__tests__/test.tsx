import React from 'react';
import {mount} from 'enzyme';
import Home from "../../containers/home/component";
import Category from "../../containers/category/component";
import Help from "../../containers/help/component";
import {Provider} from 'react-redux';
import store from '../../store';
import mockAdapter from "axios-mock-adapter";
import { multipleRequest } from "../utils";
import axios from "axios";

import {StaticRouter} from "react-router-dom";

import {describe, it, expect, beforeEach} from "@jest/globals";


describe("<Category />", () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(axios);
    });

    it("components", async () => {
        const wrapper = mount(<StaticRouter><Provider store={store}><Category/></Provider></StaticRouter>);

        const dataResponse = [["GET", "/api/venues/categories?client_id=FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ&client_secret=ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W&v=20201202",
            {},
            200,
            require('../../../stubs/endpoint/success.json')]];

        await multipleRequest(mockAxios,dataResponse);

        wrapper.update();

        mockAxios.reset();

        wrapper.find('span#targetId').at(1).simulate('click');
        wrapper.find('p#subId').at(0).simulate('click');

        const dataResponseSub = [["GET", "/api/venues/search?near=moscow&categoryId=4fceea171983d5d06c3e9823&client_id=FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ&client_secret=ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W&v=20201202",
            {},
            200,
            require('../../../stubs/endpoint/subCategorySuccess.json')]];

        await multipleRequest(mockAxios,dataResponseSub);

        wrapper.update();

        mockAxios.reset();
        wrapper.find('span#targetId').at(0).simulate('click');
        const dataResponseCat = [["GET", "/api/venues/search?near=moscow&categoryId=4fceea171983d5d06c3e9823&client_id=FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ&client_secret=ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W&v=20201202",
            {},
            200,
            require('../../../stubs/endpoint/subCategorySuccess.json')]];

        await multipleRequest(mockAxios,dataResponseCat);

        wrapper.update();

        expect(wrapper).toMatchSnapshot();
    });
})

describe("<Home />", () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(axios);
    });
    it("components", async () => {
        const wrapper = mount(<StaticRouter><Provider store={store}><Home/></Provider></StaticRouter>);

        wrapper.find('button#onSearchHome').simulate('click');

        const dataResponse = [["GET", "/api/venues/search?near=moscow&radius=10000&limit=10&query=pizza&client_id=FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ&client_secret=ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W&v=20201202",
            {},
            200,
            require('../../../stubs/endpoint/subCategorySuccess.json')]];

        await multipleRequest(mockAxios,dataResponse);

        wrapper.update();
        expect(wrapper).toMatchSnapshot();
    });
})

describe("<Help />", () => {
    it("components", () => {
        const wrapper = mount(<StaticRouter><Provider store={store}><Help/></Provider></StaticRouter>);
        expect(wrapper).toMatchSnapshot();
    });
})




// const add = require("../add");
//
// describe("проверяем сложение", () => {
//     it("Sum numbers", () => {
//         expect(add(2,1)).toBe(3);
//     });
// })
