import React from 'react';
import style from './style.css';
import {Switch, Route} from 'react-router-dom';
import Home from './containers/home/component';
import Category from './containers/category/component';
import Help from "./containers/help/component";
import {Menu} from './component/index';

import {URLs} from "./urls";

const App = () => (
    <div className={style.app}>
        <div>
            <Menu/>
        </div>
        <div className={style.pages}>
            <Switch>
                <Route exact component={Home} path={URLs.root.url}/>
                <Route component={Category} path={URLs.category.url}/>
                <Route component={Help} path={URLs.help.url}/>
            </Switch>
        </div>
    </div>
);

export default App;