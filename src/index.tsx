import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import store from './store';
import i18next from 'i18next';
import { i18nextReactInitConfig } from '@ijl/cli';
import { BrowserRouter } from 'react-router-dom';
i18next.t = i18next.t.bind(i18next);

const i18nextPromise = i18nextReactInitConfig(i18next);

export default () => <App />;

export const mount = async (Сomponent) => {
    await Promise.all([i18nextPromise]);
    ReactDom.render(
        <BrowserRouter basename="/geo.guide">
        <Provider store={store}>
            <Сomponent />
        </Provider>
        </BrowserRouter>,
        document.getElementById('app')
    );
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};
