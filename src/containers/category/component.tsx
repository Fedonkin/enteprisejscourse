import React from 'react';
import style from './style.css';
import {Input, Button, Tooltip, notification} from 'antd';
import cn from 'classnames';
import {connect} from 'react-redux';

import {
    getCategoryDataRequest,
    getCategoryListDataRequest,
    getSubCategoryDataRequest,
} from '../../models/category/action';
import {RouteProps} from 'react-router';
import {TabCategory} from '../../component/index';
import {QuestionCircleOutlined, SmileOutlined} from "@ant-design/icons";
import i18next from "i18next";


interface CategoryProp {
    categories: any;
    categoryId?: any;
    isCategory?: boolean;
    categoriesItem?: any;
    getCategoryIdData?: any;
    city?: string;
}

class Category extends React.Component<RouteProps, CategoryProp> {
    state = {
        categories: [],
        categoriesItem: [],
        categoryId: undefined,
        isCategory: false,
        count: 0,
        city: '',
    };


    componentDidMount() {
        this.props.getCategoryListData();
    }

    onCategories = (e) => {
        const {getCategoryData} = this.props;
        const categoryId = e.target.id;
        const {city} = this.state;
        const {t} = i18next;

        if (!city) {
            notification.open({
                message: t('required-fields'),
                description:
                    t('none-city'),
                icon: <SmileOutlined style={{color: '#108ee9'}}/>,
            });
        } else {
            getCategoryData(city, categoryId);
        }
    };

    onSubcategories = (e) => {
        const categoryId = e.target.id;

        this.setState({categoryId: categoryId, isCategory: true});
    };

    onCancel = () => {
        this.setState({isCategory: false});
    };

    onCityRequest = (e) => {
        this.setState({city: e.target.value});
    };
    getSubCategory = (e) => {
        const {t} = i18next;
        const {city} = this.state;
        if (!city) {
            notification.open({
                message: t('required-fields'),
                description:
                    t('none-city'),
                icon: <SmileOutlined style={{color: '#108ee9'}}/>,
            });
        } else {
            this.props.getSubCategoryData(city, e.target.id);
        }

        this.onCancel();
    };


    render() {
        const {isCategory, categoryId, city} = this.state;
        const {categoryReducer} = this.props;
        const {categoryList} = categoryReducer;
        const {t} = i18next;

        return (
            <div className={style.category}>
                <div className={style.categoryBlock}>
                    <div className={style.inputCity}>

                        <Input
                            id="onCityRequestBtn"
                            placeholder={t('city')}
                            allowClear
                            value={city}
                            onChange={this.onCityRequest}
                        />
                        <Tooltip
                            title={t('enter-city')}>
                            <QuestionCircleOutlined/>
                        </Tooltip>
                    </div>
                    <div className={style.categoryList}>
                        {categoryList.map((item) => (
                            <div key={item.id} id={item.id} className={cn(style.categoryItem)}>
                                <img
                                     className={style.icon}
                                     src={
                                        item.icon.prefix +
                                        '64' +
                                        item.icon.suffix
                                    }
                                    alt=""
                                />
                                <div>
                                    <p className={style.categoryInfo} key={item.id}>
                                        {item.name}
                                    </p>
                                    <span onClick={this.onCategories} id={item.id} className={style.categoryInfoTitle}>
                                        {t('search')}
                                    </span>
                                    <span
                                        onClick={this.onSubcategories}
                                        id={item.id}
                                        className={style.categoryInfoTitle}
                                    >
                                        {t('view-subcategory')}
                                    </span>
                                </div>
                            </div>
                        ))}

                        {isCategory &&
                        categoryId &&
                        categoryList.map((item) =>
                            categoryId == item.id ? (
                                <div className={style.categoryDetailed}>
                                    <div className={style.categoryListDetailed}>
                                        <div className={style.categoryItemDetailed}>
                                            <h1>{item.name}</h1>
                                            <img
                                                id={item.id}
                                                className={style.icon}
                                                src={
                                                    item.icon.prefix +
                                                    '64' +
                                                    item.icon.suffix
                                                }
                                                alt=""
                                            />
                                            <div className={style.categoryItemDetailedBtn}>
                                                <Button
                                                    id="onCancelSubCategory"
                                                    type="default"
                                                    onClick={this.onCancel}
                                                >
                                                    {t('back-list')}
                                                </Button>
                                            </div>
                                        </div>
                                        <div className={style.categoryDetailedInfo}>
                                            {item.categories.map((item) => (
                                                <div
                                                    key={item.id}
                                                    className={
                                                        style.subCategoryIcon
                                                    }
                                                    id={item.id}
                                                    onClick={
                                                        this.getSubCategory
                                                    }
                                                >
                                                    <img
                                                        id={item.id}
                                                        // className={style.icon}
                                                        src={
                                                            item.icon
                                                                .prefix +
                                                            '64' +
                                                            item.icon.suffix
                                                        }
                                                        alt=""
                                                    />
                                                    <p id={item.id}>
                                                        {item.name}
                                                    </p>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                ''
                            )
                        )}
                    </div>
                </div>
                <div className={style.categoryTable}>
                    <TabCategory/>
                </div>
            </div>
        );
    }
}

function mstp(state) {
    return {
        categoryReducer: state.categoryReducer,
    };
}

function mdtp(dispatch) {
    return {
        getCategoryData: (city, categoryId) => {
            dispatch(getCategoryDataRequest(city, categoryId));
        },
        getCategoryListData: () => {
            dispatch(getCategoryListDataRequest());
        },
        getSubCategoryData: (city, subCategoryId) => {
            dispatch(getSubCategoryDataRequest(city, subCategoryId));
        },
    };
}

export default connect(mstp, mdtp)(Category);
