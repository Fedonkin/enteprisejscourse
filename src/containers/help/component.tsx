import React from 'react';
import style from './style.css';
import i18next from 'i18next';


class Help extends React.Component<any, any> {
    render() {
        const {t} = i18next;
        return (
            <div className={style.help}>
                    <h3>{t('help-title')}</h3>
                    <p>{t('help-subtitle')}</p>
                    <p>{t('system-lang')}</p>
                    <p>
                        <span>{t('main')}:</span>
                        <p>{t('request-filters')}</p>
                        <p>{t('view-maps')}</p>
                    </p>

                <span>{t('category')}:</span>
                <p>{t('search-subcategory')}</p>
                <p>{t('all-search')}</p>
            </div>
        );
    }
}

export default Help;
