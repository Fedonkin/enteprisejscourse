import React from 'react';
import cn from 'classnames';
import style from './style.css';
import { Input, Button, notification, Tooltip } from 'antd';
import { Table } from '../../component';
import { connect } from 'react-redux';
import { RouteProps } from 'react-router';
import { SmileOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import i18next from "i18next";

import {
    getHomeDataRequest,
    resetHomeResultRequest,
} from '../../models/home/action';

interface HomeProp {
    id?: any;
    cityState: string;
    queryState: string;
    radiusState?: any;
    limitState?: any;
    lat?: any;
    lng?: any;
}

class HomeComponent extends React.Component<RouteProps, HomeProp> {
    constructor(props) {
        super(props);
        this.state = {
            cityState: '',
            queryState: '',
            radiusState: 10000,
            limitState: 10,
        };
    }

    onSearch = () => {
        const { cityState, queryState, radiusState, limitState } = this.state;
        const {
            getDataRequestHome,
            resetHomeResult
        } = this.props;
        const {t} = i18next;

        resetHomeResult();

        if (cityState == '' || queryState == '') {
            notification.open({
                message: t('required-fields'),
                description:
                    t('city-request'),
                icon: <SmileOutlined style={{ color: '#108ee9' }} />,
            });
        }

        // if (cityState !== '' && queryState !== '' && filters.city == undefined) {
        //     notification.open({
        //         message: 'Запрос не найден',
        //         description: 'Проверьте поля ввода или введите другой запрос и повторите попытку',
        //         icon: <SmileOutlined style={{color: '#108ee9'}}/>,
        //     });
        // }
        else {
            getDataRequestHome(cityState, queryState, radiusState, limitState);
        }

    };

    onCityState = (e) => {
        this.setState({ cityState: e.target.value });
    };
    onQueryState = (e) => {
        this.setState({ queryState: e.target.value });
    };
    onRadiusState = (e) => {
        this.setState({ radiusState: e.target.value });
    };
    onLimitState = (e) => {
        this.setState({ limitState: e.target.value });
    };

    onReset = () => {
        this.props.resetHomeResult();
        this.setState({
            cityState: '',
            queryState: '',
            radiusState: 10000,
            limitState: 10,
        });
    };

    render() {
        const { homeRequestResult } = this.props;
        const { filters, radius, limit } = homeRequestResult;

        const { cityState, queryState, radiusState, limitState } = this.state;

        const { city, query, address, lat, lng } = filters;
        const {t} = i18next;

        return (
            <div className={cn('home')}>
                <div className={cn('inputs')}>
                    <div className={style.inputsItem}>
                        <Input
                            id="city"
                            placeholder={t('city-required')}
                            onChange={this.onCityState}
                            value={cityState}
                            allowClear
                        />
                    </div>
                    <div className={style.inputsItem}>
                        <Input
                            id="query"
                            placeholder={t('city-required')}
                            onChange={this.onQueryState}
                            value={queryState}
                            allowClear
                        />
                    </div>
                    <div className={style.inputsItem}>
                        <Input
                            placeholder={t('radius')}
                            onChange={this.onRadiusState}
                            value={radiusState}
                            allowClear
                        />
                        <Tooltip title={t('distance')}>
                            <QuestionCircleOutlined />
                        </Tooltip>
                    </div>

                    <div className={style.inputsItem}>
                        <Input
                            placeholder={t('limit')}
                            onChange={this.onLimitState}
                            value={limitState}
                            allowClear
                        />
                        <Tooltip title={t('count-request')}>
                            <QuestionCircleOutlined />
                        </Tooltip>
                    </div>

                    <div className={style.btnHeader}>
                        <Button id="onSearchHome" onClick={this.onSearch}>
                            Поиск
                        </Button>
                        <Button id="onClearHome" onClick={this.onReset}>
                            Сбросить
                        </Button>
                    </div>
                </div>

                <Table
                    city={city}
                    query={query}
                    address={address}
                    radius={radius}
                    limit={limit}
                    lat={lat}
                    lng={lng}
                />
            </div>
        );
    }
}

function mstp(state) {
    return {
        homeRequestResult: state.homeRequestResult,
    };
}

function mdtp(dispatch) {
    return {
        getDataRequestHome: (city, query, radius, limit) => {
            dispatch(getHomeDataRequest(city, query, radius, limit));
        },
        resetHomeResult: () => {
            dispatch(resetHomeResultRequest());
        },
    };
}

export default connect(mstp, mdtp)(HomeComponent);
