import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("geo_guide");

export const baseUrl = navigations["geo_guide"];

export const URLs = {
    root: {
        url: navigations["geo_guide"],
    },
    category: {
        url: navigations["geo_guide_category"],
    },
    help: {
        url: navigations["geo_guide_help"],
    }
};