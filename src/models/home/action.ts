import axios from "axios";

import {getConfigValue} from "@ijl/cli";

const api = getConfigValue('api-enterpricejscourse');

const API_ID = 'FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ';
const API_SECRET = 'ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W';


export const setHomeRequestResult = (result) => {
    return {
        type: 'GET_DATA_REQUEST_RESULT',
        payload: result,
    }
};

export const setRadiusLimitRequestResult = (result) => {
    return {
        type: 'GET_RADIUS_LIMIT_REQUEST_RESULT',
        payload: result,
    }
};

export const resetHomeResultRequest = () => {
    return {
        type: 'RESET_HOME_RESULT_REQUEST',
        payload: ''
    }
};


export function getHomeDataRequest(city, query, radius, limit) {
    return (dispatch) => {
        axios.get(
            `${api}venues/search?near=${city}&radius=${radius}&limit=${limit}&query=${query}&client_id=${API_ID}&client_secret=${API_SECRET}&v=20201202`
        )
            .then((response) => dispatch(setHomeRequestResult({response, radius, limit})));
    };
}
