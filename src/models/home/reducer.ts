/*eslint no-case-declarations: "error"*/
/*eslint-env es6*/
const initialState = {
    radius: undefined,
    limit: undefined,
    filters: {}
}


export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'GET_DATA_REQUEST_RESULT': {
            const queryName = action.payload.response.data.response.venues.map((item) => item.name);
            const queryAddress = action.payload.response.data.response.venues.map((item) => item.location.address);
            const lat = action.payload.response.data.response.venues.map((item) => item.location.lat);
            const lng = action.payload.response.data.response.venues.map((item) => item.location.lng);
            return {
                ...state.filters,
                radius: action.payload.radius,
                limit: action.payload.limit,
                status: action.payload.response.data.meta.code,
                filters: {
                    city: action.payload.response.data.response.geocode.feature.displayName,
                    query: queryName,
                    address: queryAddress,
                    lat: lat,
                    lng: lng,
                },

            };
        }

        case 'RESET_HOME_RESULT_REQUEST':
            return {
                ...state,
                filters: {}
            }

        default:
            return state;
    }
}

