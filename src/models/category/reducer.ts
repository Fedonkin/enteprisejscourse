const initialState = {
    categoryList: [],
    category: [],
    categoryId: [],
    subcategories: [],
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_CATEGORY_LIST_DATA': {
            const newStateList = {...state};
            newStateList.categoryList = [
                ...action.payload.data.response.categories,
            ];
            console.log(newStateList);
            return newStateList;
        }

        case 'SET_CATEGORY_DATA_REQUEST': {
            const newState = {...state};
            newState.category = [
                ...action.payload.data.response.venues,
            ];
            return newState;
        }

        case 'SET_SUB_CATEGORY_DATA_REQUEST': {
            const newStateSubCategory = {...state};
            newStateSubCategory.subcategories = [
                ...newStateSubCategory.subcategories,
                ...action.payload.data.response.venues,
            ];
            return newStateSubCategory;
        }
        default:
            return state;
    }
}
