import axios from "axios";


import {getConfigValue} from "@ijl/cli";

const api = getConfigValue('api-enterpricejscourse');

const API_ID = 'FRNP5QG33ZHBCIWCV5TZSFCZKRHBK5KNP5ZFYQZRWJONF4HZ';
const API_SECRET = 'ESYFVZM1ZJAEHGKZJ4RM13IUH00SKDNDFY4DU3RW1HWVD45W';

export const setCategoryListData = (response) => {
    return {
        type: "SET_CATEGORY_LIST_DATA",
        payload: response
    }
}

export const setCategoryRequestResult = (result) => {
    return {
        type: 'SET_CATEGORY_DATA_REQUEST',
        payload: result,
    }
};

export const setSubCategoryRequestResult = (result) => {
    return {
        type: 'SET_SUB_CATEGORY_DATA_REQUEST',
        payload: result,
    }
};


export function getCategoryListDataRequest() {
    return (dispatch) => {
        axios.get(
            `${api}venues/categories?client_id=${API_ID}&client_secret=${API_SECRET}&v=20201202`
        )
            .then((response) => dispatch(setCategoryListData(response)));
        // .then((data) => {
        //     this.setState({ categories: data.response.categories });
        // });
    }
}


export function getCategoryDataRequest(city, categoryId) {
    return (dispatch) => {
        axios.get(
            `${api}venues/search?&near=${city}&categoryId=${categoryId}&client_id=${API_ID}&client_secret=${API_SECRET}&v=20201202`
        )
            .then((response) => dispatch(setCategoryRequestResult(response)));
    }
}
console.log(api);
export function getSubCategoryDataRequest(city, subCategoryId) {
    return (dispatch) => {
        axios.get(
            `${api}venues/search?&near=${city}&categoryId=${subCategoryId}&client_id=${API_ID}&client_secret=${API_SECRET}&v=20201202`
        )
            .then((response) => dispatch(setSubCategoryRequestResult(response)));
    }
}