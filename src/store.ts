import { createStore, combineReducers, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import homeReducer from "./models/home/reducer";
import categoryReducer from "./models/category/reducer";


const reducers = combineReducers({
    homeRequestResult: homeReducer,
    categoryReducer: categoryReducer
})

const store = createStore(reducers,
    applyMiddleware(thunk));

export default store;