import React from "react";
import cn from 'classnames';
import style from './style.css';

interface ButtonProp {
    colorScheme: string
}

const Button: React.FC<ButtonProp> = ({colorScheme, children}) => (
    <React.Fragment>
        <button className={cn(style.main, style[colorScheme])} >{children}</button>
    </React.Fragment>
)
export default Button