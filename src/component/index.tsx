import LabeledInput from './labeled-input';
import Button from './button';
import Table from './table';
import TabCategory from "./table-category";
import Menu from "./menu";
import Modal from "./modal";

export { LabeledInput, Button, Menu, Table, TabCategory, Modal };
