import React from 'react';
import { Table, Tag } from 'antd';
import './style.css';
import { RouteProps } from 'react-router';
import { connect } from 'react-redux';
import i18next from "i18next";


class TabCategory extends React.Component<RouteProps> {

    render() {
        const { category, subCategory  } = this.props;
        const {t} = i18next;


        const columns = [
            {
                title: t('city'),
                dataIndex: 'city',
                key: 'city'
            },
            {
                title: t('categoryName'),
                dataIndex: 'categoryName',
                key: 'name'
            },
            {
                title: t('categoryAddress'),
                dataIndex: 'categoryAddress',
                key: 'categoryAddress',
            },
            {
                title: t('tags'),
                key: 'tags',
                dataIndex: 'tags',
                render: (tags) => (
                    <>
                        {tags.map((tag) => {
                            let color = tag.length > 5 ? 'geekblue' : 'green';
                            if (tag === 'loser') {
                                color = 'volcano';
                            }
                            return (
                                <Tag color={color} key={tag}>
                                    {tag.toUpperCase()}
                                </Tag>
                            );
                        })}
                    </>
                ),
            },
        ];


        const data = [];
        category &&
            category.map((item) => {
                let locationCity = '';
                if (item.location.city) {
                    locationCity = item.location.city + ",";
                }
                else {
                    locationCity = ''
                }
                data.push({
                    city: locationCity  + item.location.country,
                    categoryName: item.name,
                    categoryAddress: item.location.address,
                    tags: item.categories.map((item) => item.name),
                });
            });

        subCategory &&
        subCategory.map((item) => {
            let locationCitySub = '';
            if (item.location.city) {
                locationCitySub = item.location.city + ",";
            }
            else {
                locationCitySub = ''
            }
            data.push({
                city: locationCitySub + item.location.country,
                categoryName: item.name,
                categoryAddress: item.location.address,
                tags: item.categories.map((item) => item.name),
            });
        });

        return (
            <div>
                <Table pagination={{ pageSize: 13 }} columns={columns} dataSource={data} />
            </div>
        );
    }
}

function mstp(state) {
    return {
        category: state.categoryReducer.category,
        subCategory: state.categoryReducer.subcategories
    };
}

function mdtp() {
    return {};
}

export default connect(mstp, mdtp)(TabCategory);
