import React from 'react';
import cn from "classnames";
import style from "./style.css";

interface LabeledInputProps {
    id: string | number;
    name: string;
    type?: string;
    title: string;
    colorScheme?: string;
}

export const LabeledInput = ({title, id, name, type = 'text', colorScheme}: LabeledInputProps) => (
    <React.Fragment>
        <label className={cn(style.title)} htmlFor={String(id)}>{title}</label>
        <input className={cn(style[colorScheme])} type={type} name={name} id={String(id)}/>
    </React.Fragment>
)