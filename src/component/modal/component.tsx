import React from 'react';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import style from './style.css';
import { RouteProps } from 'react-router';
import { Button } from 'antd';
import i18next from "i18next";

class Modal extends React.Component<RouteProps> {
    state = {};

    render() {
        const { visible, onCancel, coordinates } = this.props;

        const coordinateQuery = [[...coordinates]];

        const mapData = {
            center: [...coordinates],
            zoom: 18,
        };

        const {t} = i18next;

        return (
            <div className={style.modal}>
                {visible ? (
                    <div className={style.modalWindow}>
                        <YMaps>
                            <Map
                                width="100%"
                                height="100%"
                                defaultState={mapData}
                            >
                                {coordinateQuery &&
                                    coordinateQuery.map((coordinate) => (
                                        <Placemark key="1" geometry={coordinate} />
                                    ))}
                            </Map>
                        </YMaps>
                        {/*<Input*/}
                        {/*    disabled*/}
                        {/*    style={{ width: '10%' }}*/}
                        {/*/>*/}
                        <Button
                            onClick={() => onCancel && onCancel()}
                            type="primary"
                        >
                            {t("close")}
                        </Button>
                    </div>
                ) : (
                    ''
                )}
            </div>
        );
    }
}

export default Modal;
