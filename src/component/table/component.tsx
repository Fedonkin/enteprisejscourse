import React from 'react';
import { Table, Button } from 'antd';
import style from './style.css';
import { RouteProps } from 'react-router';
import { Modal } from '../index';
import i18next from "i18next";

interface TableProps {
    isModalMaps?: boolean;
    coordinates?: any
}

class Tab extends React.Component<RouteProps, TableProps> {
    state = {
        isModalMaps: false,
        coordinates: [],
    };

    onSearchCoordinate = (record) => {
        this.setState({ coordinates: record });
        this.setState({ isModalMaps: true });
    };

    get tableCols() {
        const {t} = i18next;
        return [
            {
                title: t("city"),
                dataIndex: 'city',
                width: 400,
            },
            {
                title: t("name"),
                dataIndex: 'name',
                width: 300
            },
            {
                title: t("address"),
                dataIndex: 'address',
                width: 150,
            },
            {
                title: t("maps"),
                dataIndex: 'maps',
                width: 150,
                render: (record) => (
                    <Button
                        id="coordBtn"
                        className={style.searchMaps}
                        onClick={() => this.onSearchCoordinate(record)}
                    >
                        {t("searchMap")}
                    </Button>
                ),
            },
            {
                title: t("radius"),
                dataIndex: 'radius',
                width: 150,
            },
            {
                title: t("limit"),
                dataIndex: 'limit',
                width: 150,
            },
        ];
    }

    onCancel = () => {
        this.setState({ isModalMaps: false });
    };

    render() {
        const columns = [...this.tableCols];

        const { city, query, address, radius, limit, lat, lng } = this.props;

        const { isModalMaps, coordinates } = this.state;

        const data = [];

        if (city) {
            for (let i = 0; i < query.length; i++) {
                data.push({
                    city: city,
                    name: query[i],
                    address: address[i],
                    maps: [lat[i], lng[i]],
                    radius: radius,
                    limit: limit,
                });
            }
        }

        return (
            <div>
                <Table
                    columns={columns}
                    dataSource={data}
                    pagination={{ pageSize: 50 }}
                    scroll={{ y: 590 }}
                />

                <Modal
                    visible={isModalMaps}
                    onCancel={this.onCancel}
                    coordinates={coordinates}
                />
            </div>
        );
    }
}

export default Tab;
