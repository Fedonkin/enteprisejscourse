import React from 'react';
import { Menu, Button } from 'antd';
import {
    MailOutlined,
    AppstoreOutlined
} from '@ant-design/icons';
import i18next from 'i18next';

import { Link } from 'react-router-dom';

import { URLs } from "../../urls";
import style from './style.css';

class App extends React.Component {
    state = {
        current: 'mail',
    };

    handleClick = (e) => {
        this.setState({ current: e.key });
    };
    // onChangeLangEn = () => {
    //     const { t } = i18next;
    //     i18next.changeLanguage("en");
    // }
    onChangeLangRu = () => {
        i18next.changeLanguage("ru");
    }
    render() {
        const { current } = this.state;
        const { t } = i18next;

        return (
            <div className={style.menu}>
                <div className={style.menuList}>
                <h1>GEO.Guide</h1>
                <Menu
                    onClick={this.handleClick}
                    selectedKeys={[current]}
                    mode="horizontal"
                >
                    <Menu.Item key="main" icon={<MailOutlined />}>
                        <Link to={URLs.root.url}>{t("main")}</Link>
                    </Menu.Item>
                    <Menu.Item key="category" icon={<AppstoreOutlined />}>
                        <Link to={URLs.category.url}>{t("category")}</Link>
                    </Menu.Item>
                    <Menu.Item key="help" icon={<AppstoreOutlined />}>
                        <Link to={URLs.help.url}>{t("help")}</Link>
                    </Menu.Item>
                </Menu>
                    <div className={style.localesBtn}>
                    {/*<Button onClick={this.onChangeLangEn}>{t("en")}</Button>*/}
                    <Button onClick={this.onChangeLangRu}>{t("ru")}</Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
