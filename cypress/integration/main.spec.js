/// <reference types="cypress" />

// context('Main', () => {
//     beforeEach(() => {
//         cy.visit('http://localhost:8099/geo.guide')
//     })
//     cy.location().should((location) => {
//         expect(location.pathname).to.eq('/geo.guide')
//     })
// })
describe('main', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8099/geo.guide');
    });
    it('main test', () => {
        cy.location().should((location) => {
            expect(location.pathname).to.eq('/geo.guide');
        });

        cy.get('#city').type('Moscow').should('have.value', 'Moscow');

        cy.get('#query').type('pizza').should('have.value', 'pizza');

        cy.get('#onSearchHome').type('{enter}');
    });

    it('viewport', () => {
        cy.viewport('macbook-15');
        cy.wait(200);
        cy.viewport('macbook-13');
        cy.wait(200);
        cy.viewport('macbook-11');
        cy.wait(200);
        cy.viewport('ipad-2');
        cy.wait(200);
        cy.viewport('ipad-mini');
        cy.wait(200);
        cy.viewport('iphone-6+');
        cy.wait(200);
        cy.viewport('iphone-6');
        cy.wait(200);
        cy.viewport('iphone-5');
        cy.wait(200);
        cy.viewport('iphone-4');
        cy.wait(200);
        cy.viewport('iphone-3');
        cy.wait(200);
    });

    it('main error', () => {
        cy.location().should((location) => {
            expect(location.pathname).to.eq('/geo.guide');
        });

        cy.get('#city').type('error').should('have.value', 'error');

        cy.get('#query').type('test').should('have.value', 'test');

        cy.get('#onSearchHome').type('{enter}');
    });
});
