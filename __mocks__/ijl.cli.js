const { navigations, features, config } = require('../ijl.config');

module.exports = {
    getNavigations: () => {
        return {
            ...navigations,
        };
    },

    getFeatures: () => {
        return {
            ...features,
        };
    },
    getConfigValue: (keyName) => {
        return config[keyName];
    }
};
