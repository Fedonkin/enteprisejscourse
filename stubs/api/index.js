const router = require('express').Router();


router.get('/venues/categories', (req,res) => {
    res.send(require('../endpoint/success.json'));
})

router.get('/venues/search', (req,res) => {
    res.send(require('../endpoint/subCategorySuccess.json'));
})

module.exports = router;
